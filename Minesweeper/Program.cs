﻿/// <summary>
/// This work has been dedicated to Our Lord, Jesus Christ.
/// 
///			||
///			||
///		[][][][][]
///			||
///			||
///			||
///			||
/// 
/// </summary>
namespace Minesweeper
{
	class Program
	{
		static void Main(string[] args)
		{

			Game game = new Game();
			game.Start();

		}
	}
}

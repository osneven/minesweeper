﻿using System;
using System.Text;
using System.Threading;

namespace Minesweeper
{
	/// <summary>
	/// Contains the functions needed for rendering the game to the screen.
	/// </summary>
	class Renderer
	{
		private Game game;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="game">The game to print for.</param>
		public Renderer(Game game)
		{
			this.game = game;
		}

		/// <summary>
		/// The width of each line that will be cleared upon re-render.
		/// </summary>
		public const int CLEAR_WIDTH = 40;

		/// <summary>
		/// Prints all the elements of the game to the screen.
		/// </summary>
		/// <param name="board">The board to render.</param>
		/// <param name="cursor">The location of the player's cursor.</param>
		/// <param name="revealBombs">Whether to reveal all the bombs on the print or not.</param>
		public void PrintFrame(Board board, Vector2<int> cursor, bool revealBombs)
		{
			string ln = new string(' ', CLEAR_WIDTH) + '\n';
			StringBuilder screen = new StringBuilder(new string('\n', 3));

			// Add the board to the screen.
			AddBoardToScreen(board, cursor, screen, revealBombs);

			// Print the screen.
			Console.SetCursorPosition(0, 0);
			Console.WriteLine(screen.ToString());
		}

		/// <summary>
		/// Blinks the last frame a bit, so the user can see he lost, and then prints the game over screen with prompt for exit or replay.
		/// </summary>
		/// <param name="board"></param>
		public void PrintGameOver(Board board)
		{
			BlinkBoard(board);

			Console.WriteLine(new string('\n', board.Height / 2 - 1));
			Console.WriteLine("     GAME OVER!");
			Console.WriteLine($"     Time: {GetTimeString(game.CurrentTime)}");
			Console.WriteLine("\n     Play again (y) or exit (n)?");
		}

		/// <summary>
		/// Blinks the last frame a bit, so the user can see he won, and then prints the game over/win screen with prompt for exit or replay.
		/// </summary>
		/// <param name="board"></param>
		public void PrintGameWin(Board board)
		{
			BlinkBoard(board);

			Console.WriteLine(new string('\n', board.Height / 2 - 1));
			Console.WriteLine("     YOU WON!");
			Console.WriteLine($"     Time: {GetTimeString(game.CurrentTime)}");
			Console.WriteLine("\n     Play again (y) or exit (n)?");
		}

		/// <summary>
		/// Prints the current game time span to the screen.
		/// </summary>
		/// <param name="time">The time to print.</param>
		public void PrintCurrentTime(TimeSpan time)
		{
			Console.SetCursorPosition(0, 0);
			Console.WriteLine($"\n Time: {GetTimeString(time)}");
		}

		/// <summary>
		/// Returns the time in the correct format.
		/// </summary>
		/// <param name="time">The time to use.</param>
		/// <returns>The time string.</returns>
		private string GetTimeString(TimeSpan time)
		{
			return $"{time.Hours}:{time.Minutes}:{time.Seconds}";
		}

		/// <summary>
		/// Adds the print of the game board to the screen.
		/// </summary>
		/// <param name="board">The board to add to the print.</param>
		/// <param name="cursor">The location of the player's cursor.</param>
		/// <param name="screen">The string builder to append it to.</param>
		/// <param name="revealBombs">Whether to reveal all the bombs on the print or not.</param>
		private void AddBoardToScreen(Board board, Vector2<int> cursor, StringBuilder screen, bool revealBombs)
		{
			// Go through the rows of the board.
			for (int i = 0; i < board.Height; i++)
			{
				// Go through the columns of the board.
				for (int j = 0; j < board.Width; j++)
				{
					char render;
					Cell cell = board.Cells[i, j];

					// If the player's cursor is on the cell, render it as such.
					if (cursor.Equals(j, i))
						render = 'o';

					// If the cell is marked, render it as such.
					else if (cell.IsMarked)
						render = '*';

					// Else if the cell is hidden, render it as such.
					else if (cell.IsHidden)
						if (cell.IsBomb && revealBombs)
							render = 'B';
						else
							render = '~';

					// Else if the cell is a bomb, display it as such.
					else if (cell.IsBomb)
						render = 'B';

					else
					{
						// Else if the cell should display a clue, render it as such.
						int clue = board.GetCellClue(new Vector2<int>(j, i));
						if (clue > 0)
							render = clue.ToString()[0];

						// Otherwise the cell is empty, so display it as such.
						else
							render = '.';
					}

					// Add the char to the screen.
					screen.Append($" {render}");
				}

				// Add a line break to the screen.
				screen.Append('\n');
			}
		}

		/// <summary>
		/// Blinks the board a bit on the screen.
		/// </summary>
		/// <param name="board">The board to blink.</param>
		private void BlinkBoard(Board board)
		{
			for (int i = 0; i < 11; i++)
			{
				if (i % 2 == 0)
					Console.Clear();
				else
				{
					PrintFrame(board, new Vector2<int>(-1, -1), true);
					PrintCurrentTime(game.CurrentTime);
				}
				Thread.Sleep(600);
			}
		}
	}
}

﻿using System;
using System.Threading;

namespace Minesweeper
{
	/// <summary>
	/// The game class combining the input, the game board, the game logic and the game rendering.
	/// </summary>
	class Game
	{
		/// <summary>
		/// The player's cursor.
		/// </summary>
		public Vector2<int> Cursor;

		private Board board;
		private Renderer renderer;

		private bool running, replay;

		private DateTime startTime;
		public TimeSpan CurrentTime { get; private set; }

		/// <summary>
		/// Initializes the game object and makes it ready for being run.
		/// </summary>
		public Game()
		{
			renderer = new Renderer(this);
		}





		/// <summary>
		/// Resets the data needed for a new round.
		/// </summary>
		private void ResetRound()
		{
			Console.Clear();
			startTime = DateTime.Now;

			board.InitializeCells();
			Cursor = new Vector2<int>(board.Width / 2, board.Height / 2);

			running = true;
		}

		/// <summary>
		/// Triggers a lose for the player.
		/// </summary>
		private void LoseRound()
		{
			running = false;
			renderer.PrintGameOver(board);
		}

		/// <summary>
		/// Triggers a win for the player.
		/// </summary>
		private void WinRound()
		{
			running = false;
			renderer.PrintGameWin(board);
		}



		/// <summary>
		/// Starts the game.
		/// </summary>
		public void Start()
		{
			// Prompt for and get game configurations.
			(int width, int height, int bombAmount) = Input.PromptGameConfigs();
			board = new Board(width, height, bombAmount);

			// Start the game.
			replay = false;
			do
			{
				// Reset the round.
				ResetRound();

				// Begin printing the time.
				Thread timeThread = new Thread(HandleTime);
				timeThread.Start();
				Thread.Sleep(100);

				// The main game loop. The game quits when this loop is exited.
				while (running)
				{
					// Print the current frame of the game.
					renderer.PrintFrame(board, Cursor, false);

					// If all the bombs, and only bombs, have been marked, and all cells are revealed, the player won.
					if (board.HasBombsBeenMarked() && board.IsTotallyRevealed())
						WinRound();

					// Advance to the next tick (iteration of the loop) when input is given.
					Input.HandleInput(this);
				}

				// Wait for user to either press replay or exit.
				replay = Input.GetYesOrNoInput();

			} while (replay);
		}

		/// <summary>
		/// Updates the time every half a second.
		/// </summary>
		private void HandleTime()
		{
			while (running)
			{
				// Get the current time and print it.
				CurrentTime = DateTime.Now - startTime;
				renderer.PrintCurrentTime(CurrentTime);

				// The "lowest time" is displayed as seconds, so we can safely wait half a second.
				Thread.Sleep(500);
			}
		}




		/// <summary>
		/// Moves the player's cursor. If the new position of the cursor is outside of the board, the cursor won't update.
		/// </summary>
		/// <param name="offset">The offset to move the cursor by.</param>
		public void MoveCursorSafely(Vector2<int> offset)
		{
			// Add the offset.
			Vector2<int> newPos = Cursor + offset;

			// Only update the cursor if it's still on the board.
			if (newPos.X >= 0 && newPos.Y >= 0 && newPos.X < board.Width && newPos.Y < board.Height)
				Cursor = newPos;
		}

		/// <summary>
		/// Toggles the mark on the cell where the cursor is located.
		/// </summary>
		public void ToggleMarkOnCursor()
		{
			Cell cell = GetCellOnCursor();
			cell.IsMarked = !cell.IsMarked;
		}

		/// <summary>
		/// Reveals the cell where the cursor is located, and all the adjacent empty ones as well.
		/// </summary>
		public void RevealOnCursor()
		{
			// If the cell is marked, do nothing.
			if (GetCellOnCursor().IsMarked)
				return;

			// Reveal the cell under the cursor. If it's a bomb, the player lost.
			if (board.RevealOnCell(Cursor, 0))
				LoseRound();
		}

		/// <summary>
		/// Returns the cell where the cursor is located.
		/// </summary>
		/// <returns>The cell.</returns>
		private Cell GetCellOnCursor()
		{
			return board.Cells[Cursor.Y, Cursor.X];
		}
	}
}

﻿using System;
using System.Linq.Expressions;

namespace Minesweeper.Utilities
{
	/// <summary>
	/// Contains utilities that work on generics.
	/// </summary>
	class GenericUtilities
	{

		/// <summary>
		/// Sums two generics as if they were numbers.
		/// </summary>
		/// <param name="a">The first generic value.</param>
		/// <param name="b">The second generic value.</param>
		/// <returns>The sum of a and b.</returns>
		public static T Add<T>(T a, T b)
		{
			// Declare the parameters.
			ParameterExpression paramA = Expression.Parameter(typeof(T), "a"),
								paramB = Expression.Parameter(typeof(T), "b");

			// Add the parameters together.
			BinaryExpression body = Expression.Add(paramA, paramB);

			// Compile it, call it and return the result.
			return Expression.Lambda<Func<T, T, T>>(body, paramA, paramB).Compile()(a, b);
		}

	}
}

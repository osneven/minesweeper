﻿namespace Minesweeper
{
	/// <summary>
	/// The cell of a board. Contains information about the state of the cell.
	/// </summary>
	class Cell
	{

		/// <summary>
		/// Whether this cell is hidden or if it has been discovered.
		/// </summary>
		public bool IsHidden { get; set; }

		/// <summary>
		/// Whether this cell contains a bomb or not.
		/// </summary>
		public bool IsBomb { get; private set; }

		/// <summary>
		/// Whether this cell has been marked or not.
		/// </summary>
		public bool IsMarked { get; set; }

		/// <summary>
		/// Instantiates a new hidden cell.
		/// </summary>
		/// <param name="isBomb">Whether this cell contains a bomb or not.</param>
		public Cell(bool isBomb)
		{
			IsHidden = true;
			IsBomb = isBomb;
		}

	}
}

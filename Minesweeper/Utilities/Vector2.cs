﻿using Minesweeper.Utilities;

namespace Minesweeper
{
	/// <summary>
	/// A 2D vector.
	/// </summary>
	class Vector2<T>
	{
		/// <summary>
		/// The first value in the vector.
		/// </summary>
		public T X { get; private set; }

		/// <summary>
		/// The second value in the vector.
		/// </summary>
		public T Y { get; private set; }

		/// <summary>
		/// Instantiates a new 2D vector with the given values.
		/// </summary>
		/// <param name="x">The first value in the vector.</param>
		/// <param name="y">The second value in the vector.</param>
		public Vector2(T x, T y)
		{
			X = x;
			Y = y;
		}

		/// <summary>
		/// Determines if the vector is equal to the numbers given.
		/// </summary>
		/// <param name="x">The value to compare the first value of the vector to.</param>
		/// <param name="y">The value to compare the second value of the vector to.</param>
		/// <returns>True if 'x' and 'y' equals the vector's 'X' and 'Y'.</returns>
		public bool Equals(T x, T y)
		{
			return X.Equals(x) && Y.Equals(y);
		}

		/// <summary>
		/// Determines if the given object is equal to this one.
		/// </summary>
		/// <param name="obj">The object to compare with.</param>
		/// <returns>True if the objects are equal.</returns>
		public override bool Equals(object obj)
		{
			Vector2<T> other = (Vector2<T>)obj;
			return Equals(other.X, other.Y);
		}



		/// <summary>
		/// Adds two vectors together.
		/// </summary>
		/// <param name="a">The first vector.</param>
		/// <param name="b">The second vector.</param>
		/// <returns>The vector sum of vector a and b.</returns>
		public static Vector2<T> operator +(Vector2<T> a, Vector2<T> b)
		{
			return new Vector2<T>(GenericUtilities.Add(a.X, b.X), GenericUtilities.Add(a.Y, b.Y));
		}
	}
}

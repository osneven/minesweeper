﻿using System;
using System.Linq;

namespace Minesweeper
{
	/// <summary>
	/// The board containing all the cells used in the game.
	/// </summary>
	class Board
	{
		/// <summary>
		/// The width of the board.
		/// </summary>
		public int Width { get; private set; }

		/// <summary>
		/// The height of the board.
		/// </summary>
		public int Height { get; private set; }

		/// <summary>
		/// The amount of bombs on the board.
		/// </summary>
		public int BombAmount { get; private set; }

		/// <summary>
		/// The cells contained in the board.
		/// </summary>
		public Cell[,] Cells { get; private set; }




		/// <summary>
		/// Instantiates a new board with randomly placed bombs.
		/// </summary>
		/// <param name="width">The width of the board.</param>
		/// <param name="height">The height of the board.</param>
		/// <param name="bombAmount">The amount of bombs in the board.</param>
		public Board(int width, int height, int bombAmount)
		{
			Width = width;
			Height = height;
			BombAmount = bombAmount;
		}

		/// <summary>
		/// Initializes (or overrides) the cells in the board.
		/// </summary>
		public void InitializeCells()
		{
			Cells = CreateNewCells(Width, Height, BombAmount);
		}

		/// <summary>
		/// Creates the cells of a new board (both those with and without bombs).
		/// </summary>
		/// <param name="width">The width of the board.</param>
		/// <param name="height">The height of the board.</param>
		/// <param name="bombAmount">The amount of bombs in the board.</param>
		private static Cell[,] CreateNewCells(int width, int height, int bombAmount)
		{
			Cell[,] cells = new Cell[height, width];
			Vector2<int>[] bombPositions = GetRandomBombPositions(width, height, bombAmount);

			// Go through all the rows needed in the board.
			for (int i = 0; i < height; i++)
			{
				// Go through all the columns needed in the board.
				for (int j = 0; j < width; j++)
				{
					bool isBomb = bombPositions.Contains(new Vector2<int>(j, i));
					cells[i, j] = new Cell(isBomb);
				}
			}

			return cells;
		}

		/// <summary>
		/// Returns a array of random bomb placements.
		/// </summary>
		/// <param name="width">The width of the board.</param>
		/// <param name="height">The height of the board.</param>
		/// <param name="bombAmount">The amount of bombs in the board.</param>
		/// <returns></returns>
		private static Vector2<int>[] GetRandomBombPositions(int width, int height, int bombAmount)
		{
			Vector2<int>[] bombPositions = new Vector2<int>[bombAmount];

			// Get the random positions.
			Random r = new Random();
			for (int i = 0; i < bombAmount; i++)
			{
				if (i == 0)
					bombPositions[i] = new Vector2<int>(1, 1);
				else
					bombPositions[i] = new Vector2<int>(r.Next(width), r.Next(height));
			}

			return bombPositions;
		}




		/// <summary>
		/// Calculates the clue of a cell (if it isn't a bomb).
		/// </summary>
		/// <param name="position">The position of the cell on the board.</param>
		/// <returns>The amount of bombs surrounding it (the clue).</returns>
		public int GetCellClue(Vector2<int> position)
		{
			int bombAmount = 0;

			// Go through the rows to look for bombs in (the row above the cell, the row containing the cell and the row below the cell).
			// Also make sure that the index won't go outside of the range of the array of cells.
			for (int i = Math.Max(0, position.Y - 1); i <= Math.Min(Height - 1, position.Y + 1); i++)
			{
				// Go through the columns to look for bombs in (the column above the cell, the column containing the cell and the column below the cell).
				// Also make sure that the index won't go outside of the range of the array of cells.
				for (int j = Math.Max(0, position.X - 1); j <= Math.Min(Width - 1, position.X + 1); j++)
				{
					// If the current cell contains a bomb, increment the clue.
					if (Cells[i, j].IsBomb)
						bombAmount++;
				}
			}

			return bombAmount;
		}

		/// <summary>
		/// Reveals the cell on the given position, and all the adjacent empty ones as well. Returns true if a bomb was revealed.
		/// </summary>
		/// <param name="pos">The position.</param>
		/// <param name="n">The depth counter of the recursion.</param>
		/// <returns>Returns true if a bomb was revealed.</returns>
		public bool RevealOnCell(Vector2<int> pos, int n)
		{
			Cell cell = Cells[pos.Y, pos.X];

			// If this is the initial depth of the recursion (ie. no recursion has taken place yet), 
			// and a bomb has been revealed, the player has lost the game.
			if (cell.IsBomb)
			{
				if (n == 0)
				{
					cell.IsHidden = false;
					return true;
				}
			}

			// If the cell is hidden, is empty and isn't marked, reveal it.
			else if (cell.IsHidden && !cell.IsMarked)
			{
				cell.IsHidden = false;

				// Also reveal the cells around it, if it doesn't contain a clue.
				bool hasClue = GetCellClue(pos) > 0;

				// Go through the row above, the row on and the row below the position.
				for (int i = -1; i < 2; i++)
				{
					// Go through, in a checker pattern, the column to the left of, the column on and the column to the right of the position.
					// Which effectively targets the four cells which are located 'north', 'south', 'east' and 'west' from the position.
					for (int j = -1 + Math.Abs(i % 2); j < 2; j += 2)
					{
						// If the new position is contained on the board and is hidden, reveal for it as well.
						// If, however, the starting position contains a clue, only reveal the touching cells
						// if they touch two or more revealed cells (including this one). This is done so inner-corners
						// will be revealed like outer-corners. Thus shouldn't take place if the depth of the
						// recursion is zero.
						int dx = j + pos.X, dy = i + pos.Y;
						if (dx >= 0 && dy >= 0 && dx < Width && dy < Height && Cells[dy, dx].IsHidden &&
							(!hasClue | (IsCellInnerCorner(new Vector2<int>(dx, dy)) && n > 0)))
							RevealOnCell(new Vector2<int>(dx, dy), n + 1);
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Determines whether or not the cell at a given position is an inner-corner.
		/// </summary>
		/// <param name="pos">The position of the cell.</param>
		/// <returns>True if it's an inner-corner.</returns>
		private bool IsCellInnerCorner(Vector2<int> pos)
		{
			int counter = 0;
			bool reachedRequiredAmount = false;

			//// Check if the position touches two revealed cells.

			// Go through the row above, the row on and the row below the position.
			for (int i = -1; i < 2; i++)
			{
				// Go through, in a checker pattern, the column to the left of, the column on and the column to the right of the position.
				// Which effectively targets the four cells which are located 'north', 'south', 'east' and 'west' from the position.
				for (int j = -1 + Math.Abs(i % 2); j < 2; j += 2)
				{
					// Check if the current position is contained on the board, and it isn't hidden.
					int dx = j + pos.X, dy = i + pos.Y;
					if (dx >= 0 && dy >= 0 && dx < Width && dy < Height && !Cells[dy, dx].IsHidden)
					{
						// Add another cell to the counter.
						counter++;

						// If the required amount has been hit, break out of the two loops.
						if (counter >= 2)
						{
							reachedRequiredAmount = true;
							break;
						}
					}
				}
				if (reachedRequiredAmount)
					break;
			}

			//// Check if the position has at least one corner with a revealed cell that doesn't contain a clue.
			if (reachedRequiredAmount)
			{
				// Go through the row above, the row on and the row below the position.
				for (int i = -1; i < 2; i += 2)
				{
					// Go through, in a checker pattern, the column to the left of, the column on and the column to the right of the position.
					// Which effectively targets the four cells which are located on the four corners of the targeted cell.
					for (int j = -1 + 1 - Math.Abs(i % 2); j < 2; j += 2)
					{
						// Check if the current position is contained on the board, it isn't hidden and it doesn't contain any clues.
						// If these conditions are met, the position is an inner-corner.
						int dx = j + pos.X, dy = i + pos.Y;
						if (dx >= 0 && dy >= 0 && dx < Width && dy < Height && !Cells[dy, dx].IsHidden &&
							GetCellClue(new Vector2<int>(dx, dy)) == 0)
							return true;
					}
				}
			}

			// The conditions weren't met for, and this position can't be a revealed inner-corner.
			return false;
		}

		/// <summary>
		/// Determines whether or not all cells with bombs, and cells only with bombs, have been marked.
		/// </summary>
		/// <returns>True if all the bombs have been marked.</returns>
		public bool HasBombsBeenMarked()
		{
			int markAmount = 0;

			// Go through all the rows on the board.
			for (int i = 0; i < Height; i++)
			{
				// Go through all the columns on the board.
				for (int j = 0; j < Width; j++)
				{
					// Check if the current cell is a bomb.
					Cell cell = Cells[i, j];

					// If the cell has been marked, increment the counter.
					if (cell.IsMarked)
						markAmount++;

					// If the cell isn't marked, but contains a bomb, return false.
					else if (cell.IsBomb)
					{
						// Otherwise return flase.
						return false;
					}
				}
			}

			// Determine whether all bombs, and only bombs, have been marked.
			return markAmount == BombAmount;
		}

		/// <summary>
		/// Determines whether or not all the cells on the board has been revealed.
		/// </summary>
		/// <returns>True if there are no hidden cells left on the board.</returns>
		public bool IsTotallyRevealed()
		{
			// Go through all the rows on the board.
			for (int i = 0; i < Height; i++)
			{
				// Go through all the columns on the board.
				for (int j = 0; j < Width; j++)
				{
					Cell cell = Cells[i, j];
					if (cell.IsHidden && !cell.IsMarked)
						return false;
				}
			}
			return true;
		}

	}
}

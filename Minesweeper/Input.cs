﻿using System;

namespace Minesweeper
{
	/// <summary>
	/// Handles the player's input.
	/// </summary>
	class Input
	{

		/// <summary>
		/// Waits for the player to press a input, and executes the corresponding method in the given game object.
		/// </summary>
		/// <param name="game">The game to apply the input to.</param>
		public static void HandleInput(Game game)
		{
			// Check for the input.
			switch (Console.ReadKey().Key)
			{
				//// Handle for cursor controls.
				case ConsoleKey.UpArrow:
					game.MoveCursorSafely(new Vector2<int>(0, -1));
					break;
				case ConsoleKey.DownArrow:
					game.MoveCursorSafely(new Vector2<int>(0, 1));
					break;
				case ConsoleKey.LeftArrow:
					game.MoveCursorSafely(new Vector2<int>(-1, 0));
					break;
				case ConsoleKey.RightArrow:
					game.MoveCursorSafely(new Vector2<int>(1, 0));
					break;

				//// Handle for cell activities.
				case ConsoleKey.F:
					game.ToggleMarkOnCursor();
					break;
				case ConsoleKey.Spacebar:
					game.RevealOnCursor();
					break;

				//// Handle for game exit.
				case ConsoleKey.Escape:
					Environment.Exit(1);
					break;
			}
		}

		/// <summary>
		/// Waits for the player to press yes (y) or no (n) and returns the result.
		/// All other input will be ignored until one of these are pressed.
		/// </summary>
		/// <returns>Returns true if the player pressed yes.</returns>
		public static bool GetYesOrNoInput()
		{
			while (true)
			{
				switch (Console.ReadKey().Key)
				{
					case ConsoleKey.Y:
						return true;
					case ConsoleKey.N:
						return false;
				}
			}
		}

		/// <summary>
		/// Prompts and waits for the user to type in the game configurations.
		/// </summary>
		/// <returns>The width, height and bomb amount as given by the player.</returns>
		public static (int, int, int) PromptGameConfigs()
		{
			Console.WriteLine("\nPlease enter the width, height and bomb amount you'd like, seperated by spaces.\nExample: 11 11 15\n");

			// Run until a valid answer is given.
			while (true)
			{
				// Get the input.
				string[] result = Console.ReadLine().Split(' ');

				// Try to parse it as ints.
				try
				{
					return (int.Parse(result[0]), int.Parse(result[1]), int.Parse(result[2]));
				}
				catch
				{
					Console.WriteLine("Invalid input. Try again.");
				}
			}
		}
	}
}
